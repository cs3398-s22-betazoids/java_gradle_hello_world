package hello;

//this is Jake Lopez's assignment 9 comment
//jde83 assignment 9 
//this is jde83's comment for assignment 9



//comment
public class Greeter {

  private String name = "";


  public String getName() 

  {
    return name;
  }


  public void setName(String name) 

  {
      this.name = name;
  }


  public String sayHello() 

  {
  	if (name == "") 

    {
       return "Hello!";
    }
    else 
    {
       return "Hello " + name + "!";
    }

  }

}