package hello;

// on line change

import static org.junit.jupiter.api.Assertions.*;

import javax.accessibility.AccessibleAttributeSequence;


import java.beans.Transient;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

//Robert Elizondo A9 test comment 

// Starting class (3-4)


public class TestGreeter {
    boolean flag = false;

   private Greeter g = new Greeter();
   boolean myVariable = false;

   @Test
   @DisplayName("Test for Empty Name")
   public void testGreeterEmpty() 

   {
      assertEquals(g.getName(),"");
      assertEquals(g.sayHello(),"Hello!");
   }

   @Test
   @DisplayName("Test for Ted")
   public void testGreeterTed() 
   {
      g.setName("Ted");
      assertEquals(g.getName(),"Ted");
      assertEquals(g.sayHello(),"Hello Ted!");
   }
//Robert Bonham Junit Test
@Test
@DisplayName("Test for Robbie")
public void testGreeterRobbie()
   {
      g.setName("Robbie");
      assertEquals(g.getName(),"Robbie");
      assertEquals(g.sayHello(),"Hello Robbie!");
   }

   @Test
   @DisplayName("Test for Sally")
   public void testGreeterSally() 

   {
      g.setName("Sally");
      assertEquals(g.getName(),"Sally");
      assertEquals(g.sayHello(),"Hello Sally!");
   }


   @Test
   @DisplayName("Test for Name='World'")
   public void testGreeter() 
   {
      g.setName("World");
      assertEquals(g.getName(),"World");
      assertEquals(g.sayHello(),"Hello World!");
   }

   @Test
   @DisplayName("My assertFalse test case --Robert B")
   public void assertFalseTest(){
      assertFalse(false, "this will fail." );
   }

   @Test
   @DisplayName("Test for Jacob")
   public void testGreeterJacob() 

   {
      g.setName("Jacob");
      assertEquals(g.getName(),"Jacob");
      assertEquals(g.sayHello(),"Hello Jacob!");
   }

    @Test
   @DisplayName("test for assert false")
    public void testAssertFalse(){
        assertFalse(flag,"test for assert false worked");
    }

   @Test
   @DisplayName("Test for Robert Elizondo")
   public void testGreeterRobert()
   {
      g.setName("Robert Elizondo");
      assertEquals(g.getName(),"Robert Elizondo");
      assertEquals(g.sayHello(),"Hello Robert Elizondo!");
   }

   @Test
   @DisplayName("AssertFalse test setName and getName fuctions - Robert Elizondo")
   public void testGreeterAssertFalse() 
   {
      g.setName("Robert Elizondo");
      assertFalse(g.getName().equals("Oscar"));
   }
   @Test
   @DisplayName("Test for name='Oscar'")
   public void TestGreeterOscar(){
      g.setName("Oscar");
      assertEquals(g.getName(), "Oscar");
      assertEquals(g.sayHello(), "Hello Oscar!");
   }

   @Test
   @DisplayName("AssertFalse")
   public void TestGreeterFalse(){
      g.setName("Name");
      assertFalse(g.getName().isEmpty(), "Name field is not supposed to be empty.");
      assertFalse(g.sayHello().length() == 6, "Outpur should not be Hello!");
   }

   @Test
   @DisplayName("Assignment 14 Test for Complete = Completed")
   public void TestGreeterElrod14(){
      g.setName("Complete");
      assertEquals(g.getName(), "Complete");
      assertEquals(g.sayHello(), "Hello Complete!");
   }

   @Test
   @DisplayName("A14 Test")
   public void TestGreeterA14(){
      g.setName("A14");
      assertEquals(g.getName(), "A14");
      assertEquals(g.sayHello(), "Hello A14!");
   }
    
   @Test
   @DisplayName("A14 Test - Robert Elizondo")
   public void testGreeterAssertNotNull() 
   {
      g.setName("Robert Elizondo");
      assertNotNull(g);
   }
}


